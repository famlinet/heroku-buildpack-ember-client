var fs = require('fs'); 

// assuming that BUILD_DIR comes in as positional argument #1
var versionsJsonName = process.argv[2] + '/versions.json';

var versions = JSON.parse(fs.readFileSync(versionsJsonName));

/* EXAMPLE of versions.json
{
  "latest": {
    "rails-api": {
      "tag": "0.0.1",
      "commit": "abcdef"
    },
    "ember_client":{
      "tag": "0.0.0",
      "commit": "d6fd94ce336015977a7b05ad68ee5eb2cbccd5d9",
      "build_as": "development"
    }
  }
}  
*/

console.log(versions.latest.ember_client.build_as);
