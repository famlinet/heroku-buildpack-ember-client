# Heroku-Buildpack-Ember-Client

## Purpose

To support staging apps that are triggered by git clones of the Rails-Api repo.

## Heroku App Order of Operations
*	Bitbucket pipeline performs a `git push` of the Rails-Api source code to remote:Heroku.
*	SSH buildpack is loaded.
*	SSH buildpack is run, saving SSH private key and known hosts file.
*	heroku/nodejs buildpack is loaded, making Node and npm available for later operations.
*	This buildpack is loaded.
*	This buildpack runs: clones the Ember-Client source, checks out the commit of Ember-Client that is to be built in the app, builds the Ember-Client, copies the Ember-Client /dist into rails-api/public in the build directory.
*	Subsequent buildpacks complete the build process.

## Public repo

This is a public repository, as Heroku does not support accessing private repositories (requiring authentication) to clone buildpacks.
